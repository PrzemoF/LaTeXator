#!/usr/bin/python

import csv
import datetime
import sys

DEST_DIR = 'tex/'

if len(sys.argv) != 3:
    print('Usage: python generate-tex.py input.csv page-tempate.tex')
    print('Please provide input and output files!')
    quit(1)

print ('Template: ' + sys.argv[1])
print ('CSV file: ' + sys.argv[2])

today = datetime.date.today()

with open(sys.argv[2], 'rb') as source_csv_file:
    reader = csv.DictReader(source_csv_file)
    src_headers = reader.fieldnames
    for row in reader:
        dest_file_name = DEST_DIR + row['FILE-NAME'] + '.tex'
        dest_f = open(dest_file_name, 'w')
        template_f = open(sys.argv[1], 'r')
        for line in template_f:
            modified_line = line
            for field in src_headers:
                modified_line = modified_line.replace(field, row[field])

            modified_line = modified_line.replace('&', '\&')
            modified_line = modified_line.replace('_', '\_')
            modified_line = modified_line.replace('#', '\#')
            dest_f.write(modified_line)
        template_f.close()
        dest_f.close()
