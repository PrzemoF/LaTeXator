Document generator based on LaTeX, LibreOffice, python and bash.

Generates tex/pdf files based on information from LibreOffice spreadsheet and LaTeX template.

It's a quick & dirty code, might eat your dog and do other strange things. Probably it won't, but I take no responsibility for the damage :-)

Licence GPLv3, unless you need something else.

Please send the improvements back, so we can all benefit from them.


Usage:
run generate.sh to create tex/pdf files

Modify LaTeX template and input.odf to suit your needs. A column in input.odf should have matching label in LaTeX template. Be careful with characters that are special for LaTeX like _ (underscore) & or # - they should not be used in labels. Label names cannot overlap: it there is a label ITEM using ITEM-1 won't work as the python part will mess the ITEM-1 when substituting ITEM. 

TODO: More intelligent label handling to avoid problems with partial substitution (ITEM and ITEM-1)
